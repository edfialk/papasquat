<!-- resources/views/auth/reset.blade.php -->

@extends('layout')

@section('title', 'Reset Password');

@section('nav')
    @include('nav')
@stop

@section('content')

    <div class="container">

        <div class="row">

            <div class="col-sm-8 col-sm-offset-2">

                @if (count($errors) > 0)
                    <div class='alert alert-danger'>
                        @foreach ($errors->all() as $error)
                            <p class="error">{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

                <div class='panel panel-default'>
                    <div class='panel-body'>
                        <form method="POST" action="/password/reset">
                            {!! csrf_field() !!}
                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password">
                            </div>

                            <div class="form-group">
                                <label for="password_confirmation">Confirm Password</label>
                                <input type="password" class="form-control" name="password_confirmation">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Reset Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop