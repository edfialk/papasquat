<!-- resources/views/auth/password.blade.php -->

@extends('layout')

@section('title', 'Forgot Password');

@section('nav')
	@include('nav')
@stop

@section('content')

	<div class="container">

		<div class="row">

			<div class="col-sm-8 col-sm-offset-2">

			    @if (count($errors) > 0)
			        <div class='alert alert-danger'>
			            @foreach ($errors->all() as $error)
			                <p class="error">{{ $error }}</p>
			            @endforeach
			        </div>
			    @endif

			    <div class='panel panel-default'>
			    	<div class='panel-body'>
						<form action="/password/email" method="POST" role="form">

						    {!! csrf_field() !!}

						    <legend>Send Password Reset Link</legend>

						    <div class="form-group">
						        <input type="email" class="form-control" id="email" name="email" value=" {{ old('email') }}" placeholder="Email">
						    </div>

						    <button type="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop