<!DOCTYPE html>
<html lang="en">
<head>
	<title>Papa Squat - @yield('title')</title>

	<meta charset="UTF-8">
	<meta name="token" content="{{ csrf_token() }}" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="@yield('description')">

	<link rel="stylesheet" href="/css/app.css" />

	@yield('scripts.head')

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-68985973-1', 'auto');
	  ga('send', 'pageview');
	</script>

</head>
<body class="@yield('body.class')">

@yield('nav')

@yield('content')

@yield('sidebar')


<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIs1hPRRywlAXrDI1x5R3tt9QdLNyGgz4&amp;libraries=geometry"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="/js/jquery.ba-throttle-debounce.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.0/sweetalert.min.js"></script>
<script src="/js/main.js"></script>

@yield('scripts.body')

</body>
</html>