<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">Papa Squat</a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <!-- <ul class="nav navbar-nav">
        <li><a href="/">Home</a></li>
        <li><a href="/about">About</a></li>
        <li><a href="/contact">Contact</a></li>
      </ul> -->
      <ul class="nav navbar-nav navbar-right">
      	@if ( Auth::check() )
        	<p class="navbar-text">Hello {{ Auth::user()->name }}</p>
          <li><a href='/auth/logout'>Log Out</a></li>
        @else
        	<li><a href='/auth/login'>Log In</a></li>
        	<li><a href='/auth/register'>Sign Up</a></li>
        @endif
      </ul>
    </div>
  </div>
</nav> 