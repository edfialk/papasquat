@extends('layout')

@section('title', "Find nearby spots to squat - in Portland")
@section('description', "Outdoor spots to sit, eat, and drink near you, whever you are. As long as you're in Portland.")

@section('content')

<div class="map">
	<div id="map-canvas"></div>
</div>

<div class='sidebar-toggle hamburger hamburger-toggle'>
	<span class='first'></span>
	<span class='second'></span>
	<span class='third'></span>
</div>

<div class='panel panel-default panel-search'>
	<form class='form-inline form-search'>
		<div class='form-group input-group'>
			<input type='text' id='search' class='form-control' placeholder='Search Papa Squat'>
		</div>
		<div class='form-group loading-group'>
			<img src='/img/gps-loading.gif'>
		</div>
		<div class='form-group search-group hidden'>
			<span class='glyphicon glyphicon-search'></span>
		</div>
		<div class='form-group remove-group hidden'>
			<span class='glyphicon glyphicon-remove'></span>
		</div>
	</form>
</div>

<div class='panel panel-default panel-list-wrapper list-open'>
	<div class='panel-list'>
		<div class='panel-heading'>
			<div class="dropdown">
			  <a class="dropdown-toggle" id="countmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    <span class='text'>10</span>
			    <span class="caret"></span>
			  </a>
			  <ul class="dropdown-menu" aria-labelledby="countmenu">
			    <li class='active'><a href="#" data-count='10'>10</a></li>
			    <li><a href="#" data-count='20'>20</a></li>
			    <li><a href="#" data-count='50'>50</a></li>
			    <li><a href="#" data-count='0'>All</a></li>
			  </ul>
			</div>
			<div class="dropdown">
			  <a class="dropdown-toggle" id="sortmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    <span class='text'>Closest</span>
			    <span class="caret"></span>
			  </a>
			  <ul class="dropdown-menu" aria-labelledby="sortmenu">
			    <li class='active'><a href="#" data-sort='distance'>Closest</a></li>
			    <li><a href="#" data-sort='rating'>Best</a></li>
			  </ul>
			</div>
			<div class="dropdown">
			  <a class="dropdown-toggle" id="typemenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    <span class="text">Spots</span>
			    <span class="caret"></span>
			  </a>
			  <ul class="dropdown-menu" aria-labelledby="typemenu">
			    <li class='active'><a href="#">Spots</a></li>
			    <li><a href="#">Parks</a></li>
			    <li><a href="#">Restaurants</a></li>
			    <li><a href="#">Bars</a></li>
			  </ul>
			</div>
			<div id="radius-dropdown" class="dropdown hidden">
			  <a class="dropdown-toggle" id="radiusmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    <span class="text">1 Mile</span>
			    <span class="caret"></span>
			  </a>
			  <ul class="dropdown-menu" aria-labelledby="radiusmenu">
			    <li class="active"><a href="#" data-radius="1">1 Mile</a></li>
			    <li><a href="#" data-radius="2">2 Miles</a></li>
			    <li><a href="#" data-radius="5">5 Miles</a></li>
			    <li><a href="#" data-radius="0.4">4 Blocks</a></li>
			  </ul>
			</div>
			<a href='#feature-modal' data-toggle="modal" data-target="#feature-modal">More</a>
			<a href='#' class='reset-filters'>Reset</a>
		</div>
		<div class='panel-body'>
			<div class='list'></div>
		</div>
		<div class='panel-footer'>
			<div class='pull-left list-collapse'>
				<span class='glyphicon glyphicon-menu-up'></span>
  				<span class='toggle-text'>Show List</span>
			</div>
			<div class='pull-right pages'>
				<span class='start'>1</span> - <span class='end'>10</span>
				<span class='glyphicon glyphicon-menu-left prev disabled'></span>
				<span class='glyphicon glyphicon-menu-right next'></span>
			</div>
			<div class='clearfix'></div>
		</div>
	</div>
</div>

<div class="modal fade" id="feature-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Features</h4>
      </div>
      <div class="modal-body">
      	<div class='container-fluid filters'>
			<div class='all-tags row'>
				<div class='column col-xs-12 col-sm-4'></div>
				<div class='column col-xs-12 col-sm-4'></div>
				<div class='column col-xs-12 col-sm-4'></div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default clear-filters">Clear</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="save-filters">Filter</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="empty alert alert-warning hidden">No results. Try fewer <a href='#feature-modal' data-toggle="modal" data-target="#feature-modal">filters</a>.</div>

@stop

@section('sidebar')
	@include('sidebar')
@stop

@section('scripts.body')

<script type="text/javascript" src="js/home.js"></script>

@stop