<div class='sidebar-overlay'></div>

<aside id="sidebar" class="sidebar sidebar-default sidebar-fixed-left" role="navigation">

    <div class="sidebar-header header-cover">
        <span class='sidebar-brand'>Papa Squat</span>
        @if (Auth::check())
            <a href='#' class="sidebar-user-link" >
	            {{ Auth::user()->name }}
	        </a>
        @endif
    </div>

    <ul class="nav sidebar-nav">
        @if (!Auth::check())
        	<li><a href='/auth/login'>Login</a></li>
        	<li><a href='/auth/register'>Register</a></li>
        @else
            <li><a href='/create'>Add Location</a></li>
        @endif
        <li>
            <a href="http://edfialk.com">Contact</a>
        </li>
        @if (Auth::check())
            <li>
                <a href='/auth/logout'>Log out</a>
            </li>
        @endif
    </ul>

    <div class="sidebar-footer text-center">
        <span class='glyphicon glyphicon-copyright-mark'></span> {{ date("Y") }} <a href='http://edfialk.com'>Ed Fialkowski</a> All Rights Reserved
    </div>

</aside>