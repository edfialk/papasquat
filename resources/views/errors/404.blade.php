@extends('layout')

@section('body.class', 'error-page')

@section('nav')
	@include('nav')
@stop

@section('content')

	<div class="container">
	    <div class="row">
	        <div class="col-md-12">
	            <div class="text-center">
	                <h1>Oops!</h1>
	                <h2>404 Not Found</h2>
	                <p class="error-details">
	                    Sorry, an error has occured, Requested page not found!
	                </p>
	                <p class="error-actions">
	                    <a href="/" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-home"></span> Take Me Home </a>
	                    <a href="http://www.edfialk.com" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Support </a>
	                </p>
	                <p><a href='http://www.freepik.com/free-vector/green-grass-background-with-sunshine_776941.htm'>Designed by Freepik</a></p>
	            </div>
	        </div>
	    </div>
	</div>

@stop