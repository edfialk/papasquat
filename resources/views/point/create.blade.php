@extends('layout')

@section('nav')
	@include('nav')
@stop

@section('title', 'Add a Location')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
				@if (count($errors) > 0)
					<div class='alert alert-danger'>
					    @foreach ($errors->all() as $error)
					        <p class="error">{!! $error !!}</p>
					    @endforeach
					</div>
				@endif

				<form action="/create" method="POST" role="form" class='create-form'>
					<legend>Add a new location <small>* = required</small></legend>

					{!! csrf_field() !!}

					<div class="form-group">
						<label for="city">City</label>
						<input type="text" class="form-control" id="city" name="city" placeholder="City" value="Portland" required readonly >
					</div>

					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="Name *" value="{{ Input::old('name') }}" required >
					</div>

					<div class="form-group">
						<label for="website">Website</label>
						<input type="url" class="form-control" id="website" name="website" placeholder="Website" value="{{ Input::old('website') }}">
					</div>

					<div class="form-group">
						<label for="description">Description</label>
						<textarea  rows=1 class="form-control" id="description" name="description" placeholder="Description">{{ Input::old('description') }}</textarea>
					</div>

					<div class="form-group">
						<label for="tags"><a href='#tag-modal' data-toggle="modal" data-target="#tag-modal">Tags</a></label>
						<input type="text" class="form-control" id="tags" name="tags" placeholder="Tags" readonly >{{ Input::old('tags') }}
					</div>

					<div class="form-group">
						<label for="address">Address</label>
						<input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{ Input::old('address') }}" >
						<p>Click <a href="#" class="link-geocode">here</a> to get the coordinates for this address.</p>
					</div>

					<div class='row'>

						<div class="form-group col-xs-6">
							<label for="lat">Latitude *</label>
							<input type="text" class="form-control" id="lat" name="lat" value="{{ Input::old('lat') }}" required pattern="-?\d{1,3}\.\d+">
							<span class="help-block hidden">Must be a number between -90 and 90.</span>
						</div>

						<div class="form-group col-xs-6">
							<label for="lng">Longitude *</label>
							<input type="text" class="form-control" id="lng" name="lng" value="{{ Input::old('lng') }}" required pattern="-?\d{1,3}\.\d+">
							<span class="help-block hidden">Must be a number between --180 and 180.</span>
						</div>

					</div>

					<p>Click map to set location.</p>
					<div class="map-create panel panel-default">
						<div id='map-canvas'></div>
					</div>


					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>


	<div class="modal fade" id="tag-modal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Tags</h4>
	      </div>
	      <div class="modal-body">
	      	<div class='container-fluid filters'>
				<div class='all-tags row'>
					<div class='column col-xs-12 col-sm-4'></div>
					<div class='column col-xs-12 col-sm-4'></div>
					<div class='column col-xs-12 col-sm-4'></div>
				</div>
			</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default clear-tags hidden">Clear</a>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	        <button type="button" class="btn btn-primary" id="save-tags">Set</button>
	      </div>
	    </div>
	  </div>
	</div>

@stop

@section('scripts.body')

<script type="text/javascript" src="js/create.js"></script>

@stop