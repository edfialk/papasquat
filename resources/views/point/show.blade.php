@extends('layout')

@section('scripts.head')
	<link rel="stylesheet" href="/css/dropzone.css">
@stop

@section('title', $point->name )
@section('description', $point->description )

@section('nav')
	@include('nav')
@stop

@section('content')
<div class='container'>
	<h2 class='name'>{{ $point->name }}</h2>

	<div class='rating'>
	    <div id="{{ $point->id }}" data-rating="{{ $point->rating }}" data-votes="{{ $point->votes }}" class="rate_widget">
	        <div class="star_1 ratings_stars"></div>
	        <div class="star_2 ratings_stars"></div>
	        <div class="star_3 ratings_stars"></div>
	        <div class="star_4 ratings_stars"></div>
	        <div class="star_5 ratings_stars"></div>
	        <div class="total_votes">{{ $point->presentVotes() }}</div>
	    </div>
	</div>

	<div class="description">{!! $point->description !!}</div>

	@if ($point->tags->count() > 0)
		<h4>
			@foreach($point->tags as $tag)
				<span class='badge'>{{ $tag->name }}</span>
			@endforeach
		</h4>
	@endif

	<h4 class='address'>{{ $point->address or ''}}</h4>

	<ul class='links'>
		<li><a href='https://www.google.com/maps/dir/Current+Location/{{ $point->lat }},{{ $point->lng }}'>directions</a></li>
		@if ($point->website)
			<li>
				<a rel="nofollow" href='{{ $point->website }}'>{{ $point->presentWebsite() }}</a>
			</li>
		@endif
		<li><a href='https://www.google.com/search?q={{ $point->city }}+{{ urlencode($point->name) }}'>search google</a></li>
	</ul>



	<div class='row'>
		<div class='col-xs-12 col-md-6 col-lg-4'>
			<div class="map-small panel panel-default">
				<div id='map-canvas'></div>
			</div>
		</div>
		<div class='col-xs-12 col-md-6 col-lg-8 pictures'>
			<div class='gallery row'>
				@foreach ($point->pictures as $picture)
					<a href='/img/point/{{ $picture->filename }}'>
						<img src='/img/point/tn-{{ $picture->filename }}' class='col-xs-4' title='{{ $picture->caption }}'>
					</a>
				@endforeach
			</div>
		</div>
	</div>

	<div class='row'>
		<div class='col-xs-12 add-photo-wrapper'>
			@if (Auth::check())
				<a class='btn btn-default' data-toggle="modal" href='#add-photo-modal'>Add Photo</a>
				<a class='btn btn-default' href='#'>Add Comment</a>
			@else
				<a class='btn btn-default' href='/auth/login?redirect={{ $point->city }}/{{ $point->slug }}'>Add Photo</a>
				<a class='btn btn-default' href='/auth/login?redirect={{ $point->city }}/{{ $point->slug }}'>Add Comment</a>
			@endif
		</div>
	</div>



	<div class="modal fade" id="add-photo-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add Photo</h4>
				</div>
				<div class="modal-body">
					<div role="tabpanel">
					    <ul class="nav nav-tabs" role="tablist">
					        <li role="presentation" class="active">
					            <a href="#tab-upload" aria-controls="tab-upload" role="tab" data-toggle="tab">upload</a>
					        </li>
					        <li role="presentation">
					            <a href="#tab-web-address" aria-controls="tab-web-address" role="tab" data-toggle="tab">web address</a>
					        </li>
					    </ul>
					    <!-- Tab panes -->
					    <div class="tab-content">
					        <div role="tabpanel" class="tab-pane active" id="tab-upload">
								<form action="{{ $point->getUrl() }}/photo" class="dropzone" id="dropzone">
								  <div class="fallback">
								    <input name="file" type="file" multiple />
								  </div>
								</form>
					        </div>
					        <div role="tabpanel" class="tab-pane" id="tab-web-address">
								<form action="{{ $point->getUrl() }}/photo" role="form" class="form">
									<div class="form-group">
										<input type="url" class="form-control" id="url" placeholder="Paste Image URL here">
										<button type="button" class="btn btn-primary hidden" id="submit-web-address">Submit</button>
									</div>
								</form>
								<div class='alert alert-danger status hidden'></div>
					        </div>
					    </div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts.body')

	<input type="hidden" id="location-lat" value="{{ $point->lat }}" />
	<input type="hidden" id="location-lng" value="{{ $point->lng }}" />
	<input type="hidden" id="csrf" value="{{ $csrf }}" />
	<input type="hidden" id="vote" value="{{ $point->getVoteFromUser() }}" />

	@if (!Auth::check())
		<input type='hidden' id='redirectLoc' value="{{ $point->city }}/{{ $point->slug }}" />
	@endif

	<script type="text/javascript" src="/js/dropzone.min.js"></script>
	<script type="text/javascript" src="/js/lib/magnific-popup.min.js"></script>
	<script type="text/javascript" src="/js/show.js"></script>

@stop