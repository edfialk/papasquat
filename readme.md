## Papa Squat

For finding outdoor spots to sit, eat, and drink.

Live site: http://papasquat.net

Built on Laravel 5.1

### License

This site is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)