<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/fix', 'PictureController@fix');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


Route::get('near/{lat}/{lng}', 'PointController@near');
Route::get('{city}/tags', 'TagController@cityTags');
Route::get('{city}/{name}', 'PointController@show');
Route::post('{city}/{name}/rate', ['middleware' => 'auth', 'uses' => 'PointController@rate']);
Route::post('{city}/{name}/photo', ['middleware' => 'auth', 'uses' => 'PointController@addPhoto']);

Route::get('create', ['middleware' => 'auth', 'uses' => 'PointController@create']);
Route::post('create', ['middleware' => 'auth', 'uses' => 'PointController@store']);

Route::resource('point', 'PointController');
Route::resource('tags', 'TagController'); //!!


