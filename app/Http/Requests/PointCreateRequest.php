<?php

namespace App\Http\Requests;

use Auth;

use App\Http\Requests\Request;

class PointCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'city' => 'required|string',
            'description' => 'string',
            'website' => 'url',
            'tags' => 'string'
        ];
    }
}
