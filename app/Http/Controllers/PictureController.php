<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Picture;

use Image;

class PictureController extends Controller
{

    public function fix()
    {

        $upload_dir = env('PHOTO_UPLOAD_DIR', 'img/point');
        $thumb_prefix = env('THUMBNAIL_PREFIX', 'tn-');

        foreach (Picture::all() as $picture) {
            if (!file_exists($upload_dir."/".$thumb_prefix.$picture->filename)){
                if (file_exists($upload_dir."/".$picture->filename)){
                    $thumb = Image::make($upload_dir."/".$picture->filename)
                        ->fit(200,200)
                        ->save($upload_dir."/".$thumb_prefix.$picture->filename);
                }else{
                    $picture->delete();
                }
            }
        }

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
