<?php

namespace App\Http\Controllers;

use \Auth;
use \DB;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\Requests\PointCreateRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Tag;
use App\Point;
use App\Picture;
use App\Rating;

class PointController extends Controller
{

    protected $fillable = ['city', 'name', 'description', 'lat', 'lng', 'address', 'website'];

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

/*    public function getXPath($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $html = curl_exec($ch);
        curl_close($ch);

        $doc = new \DOMDocument();
        @$doc->loadHTML($html);
        $xpath = new \DOMXpath($doc);
        return $xpath;
    }*/

    public function near(Request $request, $lat, $lng)
    {
        $sort = $request->input('sort', 'distance');
        $dir = $request->input('dir', 'asc');
        $type = $request->input('type', 'all');
        $radius = $request->input('radius', 1);
        $start = $request->input('start', 0);
        $limit = $request->input('max', 0);

        $points = Point::centeredAt($lat, $lng);
        if ($type != 'all'){
            // $points->where('type', $type);
        }
        $points = $points->where('distance', '<', $radius);
        $points = $points->orderBy($sort, $dir);
        $count = $points->count();
        if ($limit !== 0){
            $points->take($limit);
        }
        $points = $points->get();
        $tags = array();
        $tag_ids = array();
        if ($start !== 0){
            $points = array_slice($points, $start);
        }
        foreach($points as &$point){
            $distance = $point->distance;
            $point = Point::find($point->id);
            $point->distance = $distance;
            $point->pictures;
            $point->tags;
            foreach ($point->tags as $t){
                if (array_search($t->id, $tag_ids) === false){
                    array_push($tags, $t);
                    array_push($tag_ids, $t->id);
                }
            }
            $thumb_prefix = env('THUMBNAIL_PREFIX', 'tn-');
            foreach($point->pictures as $pic){
                $pic->thumbnail_path = $thumb_prefix;
            }
        }

        return response()->json(['max' => $count, 'points' => $points, 'tags' => $tags], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('point.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(PointCreateRequest $request)
    {

        // $proximity = Point::where('lat', $request->lat)->where('lng', $request->lng);
        $proximity = Point::centeredAt($request->lat, $request->lng, 0.0001);
        if ($proximity->count() > 0){
            $point = $proximity->first();
            return redirect('create')
                ->withErrors(['There is already a point at that location: <a href="'.$point->getUrl().'">'.$point->name.'</a>.'])
                ->withInput();
        }

        $named = Point::where('city', $request->city)->where('name', $request->name);
        if ($named->count() > 0){
            $point = $named->first();
            return redirect('create')
                ->withErrors(['There is already a <a href="'.$point->getUrl().'">'.$point->name.'</a> in '.$point->city])
                ->withInput();
        }

        $point = new Point();

        DB::transaction(function() use ($request, $point){

            foreach($this->fillable as $key){
                $point->$key = $request->input($key);
            }

            $point->save();

            if ($request->has('tags')){
                foreach(explode(",", $request->get('tags')) as $tag){
                    $tag = Tag::firstOrCreate(['name' => $tag]);
                    $point->tags()->attach($tag);
                }
            }


        });

        return redirect($point->getUrl());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($city, $name)
    {
        $point = Point::locatedAt($city, $name)->firstOrFail();
        if (Auth::check() && Auth::user()->hasVotedOn($point)){
            $vote = $point->getVoteFromUser();
        }

        return view('point.show', compact('point', 'vote'));
    }

    /**
     * Post vote on a point
     * @return Response
     */
    public function rate($city, $name, Request $request)
    {
        if (!Auth::check()){
            return new Response('Unauthorized', 401);
        }

        $point = Point::locatedAt($city, $name)->firstOrFail();
        $point->vote($request->input('vote'));
        $point->save();

        return new Response(['rating' => $point->rating, 'votes' => $point->votes]);

    }

    public function addPhoto($city, $name, Request $request)
    {
        if (!Auth::check()){
            return new Response('Unauthorized', 401);
        }
        $point = Point::locatedAt($city, $name)->firstOrFail();

        if ($request->hasFile('file')){
            $picture = Picture::fromFile($request->file('file'));
        }else if ($request->has('url')){
            $picture = Picture::fromUrl($request->input('url'));
        }else{
            return new Response('Missing Photo', 400);
        }

        $picture->point_id = $point->id;
        $picture->user_id = Auth::user()->id;
        $picture->save();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * This was to copy and clean the oregonparks.gov description. Not sure if I still want to or not
     */
    // public function fix()
    // {
    //     $id = 1;
    //     $points = Point::where('id', '>', 2)->get();
    //     $count = 0;
    //     echo "length: ".$points->count()."<br>";
    //     foreach($points as $point){

    //         $url = $point->website;
    //         // echo $url."\n";
    //         $xpath = $this->getXPath($url);

    //         echo "<a href='".$point->getUrl()."'>".$point->name."</a><br>";

    //         $right = $xpath->query('//div[@id="parkright"]')->item(0);
    //         $amen = $xpath->query('./div[contains(text(), "Amenities")]', $right)->item(0);
    //         $divs = $xpath->query('./div', $right);
    //         $path = $amen->getNodePath();
    //         $desc = '';

    //         // echo "path: $path\n";
    //         preg_match('#\[(\d)\]$#', $path, $matches);
    //         if (count($matches) < 2){
    //             die(print_r([$url, $path]));
    //         }
    //         $index = $matches[1];
    //         for ($i = $index + 1; $i < $divs->length; $i++){
    //             // echo "i: $i\n";
    //             $div = $divs->item($i);
    //             if ($div->hasAttribute('id') && $div->getAttribute('id') == 'amenityimgs'){
    //                 continue;
    //             }
    //             if ($div->hasAttribute('class') && $div->getAttribute('class') == 'fbwrap'){
    //                 continue;
    //             }
    //             $div->removeAttribute('class');
    //             $div->removeAttribute('id');
    //             $desc .= $div->ownerDocument->saveHTML($div);
    //         }

    //         $desc = str_replace("<br><br>", "</p><p>", $desc);
    //         $desc = preg_replace('/<\/p>/', '', $desc, 1);

    //         $point->description = $desc;
    //         $point->save();
    //     }
    //     return 'done';
    // }

}
