<?php

namespace App;

use \Auth;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{

	protected $table = 'point';
	protected $fillable = ['name', 'city', 'address', 'rating', 'votes', 'lat', 'lon', 'description'];

    public function __construct($attributes = array())
    {
        $this->votes = 0;
        $this->rating = 0;

        parent::__construct($attributes);
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function pictures()
    {
        return $this->hasMany('App\Picture');
    }

    public function ratings()
    {
        return $this->hasMany('App\Rating');
    }

    public function scopeLocatedAt($query, $city, $name)
    {
    	$name = str_replace('-', ' ', $name);
    	return $query->where(compact('city', 'name'));
    }

    public function scopeFromCity($query, $city)
    {
        return $query->where(compact('city'));
    }

    public function scopeCenteredAt($query, $lat, $lng, $radius = 50)
    {
        return get_points_within($lat, $lng, $radius);
    }

    /**
     * return html display for this point's votes
     * @return string html
     */
    public function presentVotes()
    {
		return $this->votes . ' votes recorded (' . $this->rating . ' rating)';
    }

    /**
     * return html display for this point's website
     * @return string html
     */
    public function presentWebsite()
    {
        $parse = parse_url($this->website);
        return str_replace("www.", "", $parse['host']);
    }

    /**
     * get the url for this point
     * @return string url
     */
    public function getUrl()
    {
    	$name = str_replace(" ", "-", $this->name);

    	return '/'.$this->city.'/'.$name;
    }

    /**
     * set this point's name
     * @param string $name
     */
    public function setNameAttribute($name){
    	$slug = str_replace(" ", "-", $name);
    	$this->attributes['name'] = $name;
    	$this->attributes['slug'] = $slug;
    }

    public function getVoteFromUser()
    {
        if (!Auth::check()){
            return null;
        }
        if ($rating = $this->getRatingFromUser()){
            return $rating->vote;
        }
        return null;
    }
    public function getRatingFromUser()
    {
        if (!Auth::check()){
            return null;
        }
        return Rating::fromUser(Auth::user())->fromPoint($this)->first();
    }

    public function vote($vote)
    {
        $sum = $this->rating * $this->votes;
        if (Auth::user()->hasVotedOn($this)){
            $previous = $this->getRatingFromUser();
            $sum -= $previous->vote;
            $previous->vote = $vote;
            $previous->save();
        }else{
            $rating = Rating::create([
                'user_id' => Auth::user()->id,
                'point_id' => $this->id,
                'vote' => $vote
            ]);
            $this->votes = $this->votes + 1;
        }

        $sum += $vote;
        $this->rating = $sum / $this->votes;
        $this->save();
    }

}
