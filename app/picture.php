<?php

namespace App;

use Image;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{

	protected $fillable = ['filename', 'caption'];

	protected $table = 'picture';

	public static function fromFile($file){
		$upload_dir = env('PHOTO_UPLOAD_DIR', 'img/point');
		$thumb_prefix = env('THUMBNAIL_PREFIX', 'tn-');

		$photo = new static;
		$photo->filename = time()."-".$file->getClientOriginalName();

		$file->move($upload_dir, $photo->filename);

		$thumb = Image::make($upload_dir."/".$photo->filename)
			->fit(200,200)
			->save($upload_dir."/".$thumb_prefix.$photo->filename);

		return $photo;
	}

	public static function fromUrl($url){
		$upload_dir = env('PHOTO_UPLOAD_DIR', 'img/point');
		$thumb_prefix = env('THUMBNAIL_PREFIX', 'tn-');

		$photo = new static;

		$filename = explode("/", $url);
		$filename = $filename[count($filename) - 1];
		$filename = explode("?", $filename);
		$filename = $filename[0];
		$photo->filename = time()."-".$filename;
		$path = $upload_dir."/".$photo->filename;

		file_put_contents($path, file_get_contents($url));

		$thumb = Image::make($path)
			->fit(200,200)
			->save($upload_dir."/".$thumb_prefix.$photo->filename);

		return $photo;
	}

	public function point()
	{
		return $this->belongsTo('App\Point');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
