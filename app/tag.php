<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{

    public $timestamps = false;

    protected $table = 'tag';
    protected $hidden = ['pivot', 'points', 'created_at', 'updated_at'];

    public function points()
    {
    	return $this->belongsToMany('App\Point');
    }

    public function scopeFromCity($query, $city)
    {
    	return $query->where('city', $city);
    }
}
