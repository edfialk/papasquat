<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

	protected $table = 'rating';

	protected $fillable = ['vote', 'user_id', 'point_id'];

/*	public function __construct($user, $point, $rating)
	{
		$this->user_id = $user->id;
		$this->point_id = $point->id;
		$this->rating = $rating;
	}
*/
	public function scopeFromUser($query, $user)
	{
		return $query->where('user_id', $user->id);
	}

	public function scopeFromPoint($query, $point)
	{
		return $query->where('point_id', $point->id);
	}

	public function user()
	{
		$this->hasOne('App\User');
	}

	public function point()
	{
		$this->hasOne('App\Point');
	}
}
