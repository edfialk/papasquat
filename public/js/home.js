'use strict';

app = $.extend(app, {

    $empty: $('.empty').detach().removeClass('hidden'),
    $status: $('.status'),
    $list: $('.panel-list .list'),
    $listWrap: $('.panel-list-wrapper'),
    $filters: $('.filters'),
    $pages: $('.pages'),
    $loading: $('.loading-group'),
    $map: $('.map'),
    listHeightBuffer: 200,
    addPointUrl: '/create',
    foodTagId: 51,

    reset: function(){
        app.radius = 1;
        app.start = 1;
        app.count = 10;
        app.sort = 'distance';
        app.type = 'all';
        app.activeFilters = [];
    },

    initialize: function(){
        app.tags = [];
        app.points = [];
        app.reset();
        app.setListeners();
        app.initMap();

        app.$list.parent().css('max-height', $window.height() - app.listHeightBuffer);

        app.getGeo().progress(app.handleGeo).done(app.handleGeo);
    },

    handleGeo: function(){

        if (app.hasGeo){

            var distFromDefault = google.maps.geometry.spherical.computeDistanceBetween(
                new google.maps.LatLng(app.lat, app.lng),
                new google.maps.LatLng(app.defaultLat, app.defaultLng)
            );

            if (distFromDefault / 1000 > 50){
                app.lat = app.defaultLat;
                app.lng = app.defaultLng;
                app.showStatus("Sorry, I don't have anything close to you, so I'll pretend you're in downtown Portland.<br><a href='"+app.addPointUrl+"'>Help me expand.</a>");
            }
            var mypos = new google.maps.LatLng(app.lat, app.lng);
            new google.maps.Marker({
                position: mypos,
                map: app.map,
                title: 'Me!',
                icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
            });
            app.map.panTo(mypos);

        }

        app.get(0, 0, 'distance', 'all', 5).done(function(data){
            app.handleData(data);
            $('.panel-list-wrapper').show();
            app.stopLoading();
        });
/*        app.get(0, app.count, 'distance', 'all', 10).done(function(data){
            app.max = data.max;
            var points = data.points;
            app.initPoints(points);
            app.points = app.points.concat(points);
            app.addTags(data.tags);
            // app.render();
        });*/

    },

    initMap: function(){
       app.map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: { lat: app.lat, lng: app.lng },
            zoom: 15,
            mapTypeControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            }
        });
        app.map.addListener('click', function(){
            if (app.activeMarker) app.activeMarker.infoWindow.close();
            app.activeMarker = null;
            if ($(document).width() < 600 && app.$listWrap.hasClass('list-open')){
                app.toggleList();

                app.$listWrap.find('.panel-footer').one('click', function(){
                    app.toggleList();
                });
            }
        });
    },

    toggleList: function(){
        app.$listWrap.toggleClass('list-open list-closed');
        app.$map.toggleClass('expanded');
        google.maps.event.trigger(app.map, "resize");
    },

    setListeners: function(){

        $('.form-search').submit(function(e){
            app.showStatus("Sorry! Search isn't set up yet.");
            return false;
        });

        var resizeTimer;
        $(window).on('resize', function(e) {
            clearTimeout(resizeTimer);
            resizeTimer = setTimeout(function() {
                app.$list.parent().css('max-height', $window.height() - app.listHeightBuffer);
            }, 250);
        });

        $('.panel-list .panel-heading .dropdown-menu a').click(function(e){
            var $e = $(e.currentTarget);
            $e.parent().siblings('.active').removeClass('active');
            $e.parent().addClass('active');
            $e.parents('.open').removeClass('open').find('.dropdown-toggle .text').text($e.text());
        });

        $('#sortmenu').parent().find('.dropdown-menu a').click(function(e){
            var $e = $(e.currentTarget);
            app.sort = $e.data('sort');
            app.start = 1;
            var $d = $('#radius-dropdown');
            if (app.sort == 'rating'){
                $d.removeClass('hidden');
                app.radius = $d.find('.active a').data('radius');
            }else{
                $d.addClass('hidden');
                app.radius = 10;
            }

            app.sortPoints();
            app.$list.find('.item').detach();
            app.render();
            return false;
        });

        $('#radiusmenu').parent().find('.dropdown-menu a').click(function(e){
            var $e = $(e.currentTarget);
            app.radius = $e.data('radius');
            app.render();

            return false;
        });

        $('#countmenu').parent().find('.dropdown-menu a').click(function(e) {
            var $e = $(e.currentTarget);
            app.count = $e.data('count');
            var $d = $('#radius-dropdown');
            var $s = $('#sortmenu');
            app.start = 1;
            if (app.count == 0){ //"All"
                $d.removeClass('hidden');
                // $s.addClass('hidden');
                app.radius = parseInt($d.find('.active a').data('radius'));
            }else{
                if (app.sort !== 'rating'){
                    $d.addClass('hidden');
                }
                // $s.removeClass('hidden');
            }
            app.render();

            return false;
        });

        $('#search').keyup(function(){
            if ($(this).val().length == 0){
                $('.remove-group').addClass('hidden');
                $('.search-group').removeClass('with-remove');
            }else{
                $('.remove-group').removeClass('hidden');
                $('.search-group').addClass('with-remove');
            }
        });
        $('.remove-group span').click(function(){
            $('#search').val('').focus().keyup();
        });
        $('.search-group span').click(function(){
            $('.form-search').submit();
        });

        $('#save-filters').click(function(){
            app.activeFilters = app.$filters.find(':checked').map(function(i, input){ return parseInt(input.id); }).toArray();
            app.start = 1;
            app.render();
            $('#feature-modal').modal('hide');
        });
        $('.clear-filters').click(function(e){
            app.$filters.find(':checked').prop('checked', false);
            return false;
        });
        $('.reset-filters').click(function(e){
            app.reset();
            $(this).parent().find('.dropdown-menu').each(function(i, menu){
                var $m = $(menu);
                $m.find('.active').removeClass('active');
                $m.find('li').filter(":first").addClass('active');
            });
            app.$filters.find(':checked').prop('checked', false);
            $('#sortmenu .text').text('Closest');
            $('#countmenu .text').text(app.count);
            $('#typemenu .text').text('Spots');
            $('#radius-dropdown').addClass('hidden');

            app.sortPoints();
            app.$list.find('.item').detach();
            app.render();

            return false;
        });

        $('.list-collapse .glyphicon').click(function(e){
            var $p = app.$list.parent();
            if ($p.is(':visible')){
                $p.slideUp('slow');
            }else{
                $p.slideDown('fast');
            }
            $(this).toggleClass('glyphicon-menu-up glyphicon-menu-down');
            return false;
        });

        $('.sidebar-toggle').on('click', app.toggleSidebar);
        $('.sidebar-overlay').on('click', app.toggleSidebar);

        app.$pages.find('.next').click(app.nextPage);
        app.$pages.find('.prev').click(app.prevPage);

    },

    get: function(count, start, sort, type, radius){
        sort = sort.toLowerCase();
        var direction = sort == 'distance' ? 'asc' : 'desc';
        return $.getJSON('near/'+app.lat+'/'+app.lng+'?max='+count+'&start='+start+'&sort='+sort+'&dir='+direction+'&type='+type+'&radius='+radius);
    },

    handleData: function(data){
        app.max = data.max;
        app.points = data.points;
        app.initPoints(app.points);
        app.addTags(data.tags);
        app.render();
    },

    initPoints: function(ps){
        ps.forEach(function(p){
            p.display = true;
            p.$listItem = app.getListItem(p);
            p.$listItem.hover(app.onPointHoverIn, app.onPointHoverOut);
            p.marker = app.getMarker(p);
        });
    },

    setDisplay: function(point){
        point.display = true;

        if (app.count == 0 || app.sort == "rating"){
            point.display = point.distance <= app.radius;
        }

        app.activeFilters.forEach(function(tagid){
            if (!app.hasTag(point, tagid)){
                point.display = false;
            }
        });

    },

    render: function() {
        var displayPoints = [];
        var validPoints = [];
        var container = app.$list.parent();
        var bounds = new google.maps.LatLngBounds();

        app.$list.detach();
        app.hideEmpty();

        for (var i = 0; i < app.points.length; i++){

            var point = app.points[i];

            if (!$.contains(app.$list.get(0), point.$listItem.get(0))){
                app.$list.append(point.$listItem);
            }

            app.setDisplay(point);

            if (point.display){
                validPoints.push(point);
                point.$listItem.removeClass('hidden');
                if (point.marker.getMap() === null){ //prevent "blink" of re-adding marker
                    point.marker.setMap(app.map);
                }
            }else{
                point.$listItem.addClass('hidden');
                point.marker.setMap(null);
            }

            if (app.count !== 0 && point.display){ //paging
                if (validPoints.length < app.start || displayPoints.length >= app.count){
                    point.$listItem.addClass('hidden');
                    point.marker.setMap(null);
                }else{
                    displayPoints.push(point);
                }
            }else if (app.count == 0){
                displayPoints.push(point);
            }

        }

        if (displayPoints.length == 0){
            app.showEmpty();
        }else{

            if (app.count !== 0){
                app.setPager(
                    app.start,
                    Math.min(validPoints.length, app.start + app.count - 1),
                    app.start > 1,
                    app.start + app.count < validPoints.length
                );
            }else{
                app.setPager(app.start, validPoints.length, false, false);
            }

            for (var i = 0; i < Math.min(displayPoints.length, 10); i++){
                bounds.extend(displayPoints[i].position);
            }

            // app.map.fitBounds(bounds);
        }

        container.append(app.$list);
        app.$list.parent().scrollTop(0);

    },

    prevPage: function(e) {
        if ($(e.target).hasClass('disabled')) return;
        app.start = Math.max(0, app.start - app.count);
        app.render();
    },

    nextPage: function(e) {
        if ($(e.target).hasClass('disabled')) return;
        app.start = Math.min(app.max, app.start + app.count);
        app.render();
    },

    setPager: function(start, end, hasPrev, hasNext){
        app.$pages.find('.start').text(start);
        app.$pages.find('.end').text(end);
        hasPrev ? app.$pages.find('.prev').removeClass('disabled') : app.$pages.find('.prev').addClass('disabled');
        hasNext ? app.$pages.find('.next').removeClass('disabled') : app.$pages.find('.next').addClass('disabled');
    },

    clearTags: function() {
        app.$filters.find('.checkbox').remove();
    },

    addTags: function(tags){
        var tagids = app.tags.map(function(tag){ return tag.id; });
        tags.forEach(function(tag){
            if (tagids.indexOf(tag.id) === -1){
                tagids.push(tag.id);
                app.tags.push(tag);
            }
        });
        app.addTagsToPage();
    },

    addTagsToPage: function() {

        app.clearTags();

        app.tags.sort(function(a, b){
            if (a.name < b.name) return -1;
            if (b.name < a.name) return 1;
            return 0;
        });

        var $div = app.$filters.find('.all-tags'),
            numColumns = $div.children('.column').length,
            $column = $div.children('.column').first(),
            columnCount = 1,
            perColumn = Math.round(app.tags.length / numColumns),
            $clearButton = $('.clear-filters');

        for (var i = 0; i < app.tags.length; i++){
            var tag = app.tags[i],
                $li = $('<div class="checkbox"/>'),
                $label = $('<label><input type="checkbox" id="'+tag.id+'"> '+tag.name+'</label>').appendTo($li),
                $check = $label.find('input');

            if (app.activeFilters.indexOf(tag.id) !== -1) $check.prop('checked', true);

            $check.change(function(){
                app.$filters.find(':checked').length == 0 ? $clearButton.hide() : $clearButton.show();
            });

            if (i >= perColumn * columnCount ){
                $column = $column.nextAll('.column').first();
                columnCount++;
            }
            $li.appendTo($column);
        }

    },

    sortPoints: function(){
        app.points.sort(function(a, b){
            if (app.sort == 'rating'){
                if (a.rating == 0 && b.rating == 0){
                    return a.distance < b.distance ? -1 : a.distance > b.distance ? 1 : 0;
                }
                return a.rating < b.rating ? 1 : a.rating > b.rating ? -1 : 0;
            }
            return a[app.sort] < b[app.sort] ? -1 : a[app.sort] > b[app.sort] ? 1 : 0;
        });
    },

    /**
     * get google marker for point
     * @param  {object} point
     * @return {object} Google Marker
     */
    getMarker: function(point){

        point.position = new google.maps.LatLng(point.lat, point.lng);

        var marker = new google.maps.Marker({
            position: point.position,
            title: point.name,
            id: point.id,
            map: null,
            icon: '/img/tree.png'
        });

        if (app.hasTag(point, app.foodTagId)){
            marker.icon = '/img/food.png';
        }

        var rating = Math.round(point.rating * 100) / 100;
        var infoContent = '<div class="info">' +
            '<div class="name"><a href="/portland/'+point.slug+'">'+point.name+'</a></div>';
        var stars = "<div class='rating small'>"+
                "<div id='" + point.id + "' data-rating='" + point.rating + "' data-votes='" + point.votes + "' class='rate_widget'>" +
                    "<div class='star_1 ratings_stars'></div>" +
                    "<div class='star_2 ratings_stars'></div>" +
                    "<div class='star_3 ratings_stars'></div>" +
                    "<div class='star_4 ratings_stars'></div>" +
                    "<div class='star_5 ratings_stars'></div>" +
                "</div>" +
                "<div class='total_votes'>" + point.votes + " reviews</div>" +
                "<div class='clearfix'></div>" +
            "</div>";

        infoContent += stars;

        if (point.tags && point.tags.length > 0){
            infoContent += '<div>'+point.tags.map(function(tag){ return tag.name; }).join(', ')+'</div>';
        }

        if (point.address){
            infoContent += '<div>' + point.address + '</div>';
        }

        infoContent += '<div>' + ( Math.round(point.distance * 100) / 100 ) + ' miles </div>';

        infoContent += '</div>';

        var $div = $('<div/>').html(infoContent);

        var $stars = $div.find('.rate_widget');
        var rating = Math.round($stars.data('rating'));
        var $votestars = $stars.find('.star_'+rating);
        $votestars.prevAll().andSelf().addClass('ratings_vote');

        marker.infoWindow = new google.maps.InfoWindow({
            content: $div.html()
        });

        google.maps.event.addListener(marker, 'click', function(){
            if (app.activeMarker){
                app.activeMarker.infoWindow.close();
            }
            app.activeMarker = marker;
            point.marker.infoWindow.open(app.map, marker);
        });

        return marker;
    },

    /**
     * get list display for point
     * @param  {object} point
     * @return {$object} jquery object
     */
    getListItem: function(point, i){
        var $item = $('<div class="item" data-id="'+point.id+'"/>');
        var $body = $('<div class="item-body"/>').appendTo($item);
        var $left = $('<div class="item-left" />').appendTo($body);
        var $right = $('<div class="item-right" />').appendTo($body);
        if (point.pictures.length > 0){
            $('<div/>').css({
                'background-size': '80px 80px',
                'width' : '80px',
                'height' : '80px',
                'background-image': 'url(/img/point/tn-' + point.pictures[0].filename + ')'
            }).appendTo($right);
        }
        var $link = $('<span class="name"><a href="portland/'+point.slug+'">'+point.name+'</a></span>');
        $link.appendTo($left);
        var stars = "<div class='rating small'>"+
                "<div id='" + point.id + "' data-rating='" + point.rating + "' data-votes='" + point.votes + "' class='rate_widget'>" +
                    "<div class='star_1 ratings_stars'></div>" +
                    "<div class='star_2 ratings_stars'></div>" +
                    "<div class='star_3 ratings_stars'></div>" +
                    "<div class='star_4 ratings_stars'></div>" +
                    "<div class='star_5 ratings_stars'></div>" +
                "</div>" +
                "<div class='total_votes'>" + point.votes + " reviews</div>" +
                "<div class='clearfix'>" +
            "</div>";
        $left.append(stars);

        if (point.tags && point.tags.length > 0){
            // $left.append('<div>'+point.tags.map(function(tag){ return tag.name; }).join(', ')+'</div>');
        }

        if (point.address){
            $left.append('<div><span class="address">'+point.address+'</span></div>');
        }

        $item.append('<hr>');

        var $stars = $item.find('.rate_widget');
        var rating = Math.round($stars.data('rating'));
        var $votestars = $stars.find('.star_'+rating);
        $votestars.prevAll().andSelf().addClass('ratings_vote');

        return $item;
    },

    onPointHoverIn: function(e){
        var id = $(e.currentTarget).data('id');
        if (app.activePoint && app.activePoint.highlightMarker){
            app.activePoint.highlightMarker.setMap(null);
        }
        app.activePoint = app.points.filter(function(point){ return point.id == $(e.currentTarget).data('id'); })[0];
        if (typeof app.activePoint.highlightMarker == 'undefined'){
            app.activePoint.highlightMarker = new google.maps.Marker({
                position: new google.maps.LatLng(app.activePoint.lat, app.activePoint.lng),
                title: '',
                icon: google.maps.MarkerImage(
                    "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|FFFF00",
                    null, null, null,
                    new google.maps.Size(42, 68)
                )
            });
        }
        app.activePoint.marker.setMap(null);
        app.activePoint.highlightMarker.setMap(app.map);
    },

    onPointHoverOut: function(e){
        app.activePoint.highlightMarker.setMap(null);
        app.activePoint.marker.setMap(app.map);
    },

    showStatus: function(html){
        var $status = $('<div class="status alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert">×</button></div>');
        $status.append('<span>'+html+'</span>');
        $status.appendTo($('body'));
        window.setTimeout(function(){
            $('.status').fadeOut(800, function(){
                $(this).remove();
            });
        }, 5000);
    },

    toggleSidebar: function(){
        $('#sidebar').toggleClass('open');
        $('.sidebar-overlay').toggleClass('active');
        $('.hamburger-toggle').toggleClass('hamburger arrow');
    },

    hasTag: function(point, id){
        var found = false;
        point.tags.forEach(function(tag){
            if (tag.id == id){
                found = true;
            }
        })
        return found;
    },

    hideEmpty: function(){
        app.$empty.detach();
    },

    showEmpty: function(){
        app.$list.append(app.$empty);
    },

    stopLoading: function(){
        app.$loading.hide();
        app.$loading.parent().find('.search-group').removeClass('hidden');
    }

});
