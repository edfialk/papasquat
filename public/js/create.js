app = $.extend(app, {

	$lat: $('#lat'),
	$lng: $('#lng'),
	$tags: $('.all-tags'),
	geocoder: new google.maps.Geocoder(),
    $form: $('.create-form'),

    initialize: function(){

    	$.getJSON('/tags').done(function(data){
    		app.tags = data;
    		app.addTags();
    	});
    	app.setListeners();
        app.getGeo().done(function(){
        	app.$lat.val(app.lat).change();
        	app.$lng.val(app.lng).change();
	    	app.initMap();
	    });
    },

    initMap: function(){
       app.map = new google.maps.Map(document.getElementById('map-canvas'), {
            center: { lat: app.lat, lng: app.lng },
            zoom: 15,
            mapTypeControl: true,
            mapTypeControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.RIGHT_TOP
            }
        });
        app.map.addListener('click', function(e){
        	app.marker.setPosition(e.latLng);
        	app.$lat.val(e.latLng.lat());
        	app.$lng.val(e.latLng.lng());
        });
        app.marker = new google.maps.Marker({
            position: new google.maps.LatLng(app.lat, app.lng),
            map: app.map,
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        });
    },

    setListeners: function(){
    	$('.clear-tags').on('click', function(){
    		app.$tags.find(':checked').prop('checked', false);
    		$(this).addClass('hidden');
    	});
    	$('#save-tags').on('click', function(){
    		var tags = app.$tags.find(':checked').map(function(i, input){ return input.getAttribute('data-name'); }).toArray();
    		var $t = $('#tags');
    		$t.val(tags.join(','));
    		$t.parent().find('label').toggle(tags.length > 0);
            $('#tag-modal').modal('hide');
    	});
    	$('#tags').on('focus', function(){
    		$('#tag-modal').modal('show');
    	});
    	$('input').on('change', function(e){
    		var $t = $(this);
    		$t.parent().find('label').toggle($t.val().trim() != '');
    	});
    	$('textarea').on('change', function(e){
    		var $t = $(this);
    		$t.parent().find('label').toggle($t.val().trim() != '');
    	});
    	$('#lat').on('change', function(){
    		var $t = $(this);
    		var lat = Number($t.val());
    		if (isNaN(lat) || lat < -90 || lat > 90){
    			$t.parent().find('.help-block').removeClass('hidden');
    			$t.val('').focus();
    			return;
    		}
    		$t.parent().find('.help-block').addClass('hidden');
    		if (app.lat !== lat){
	    		app.lat = lat;
	    		var pos = new google.maps.LatLng(app.lat, app.lng);
	    		app.marker.setPosition(pos);
	    		app.map.setCenter(pos);
    		}
    	});
    	$('#lng').on('change', function(){
    		var $t = $(this);
    		var lng = Number($t.val());
    		if (isNaN(lng) || lng < -180 || lng > 180){
    			$t.parent().find('.help-block').removeClass('hidden');
    			$t.val('').focus();
    			return;
    		}
    		$t.parent().find('.help-block').addClass('hidden');
    		if (app.lng !== lng){
	    		app.lng = lng;
	    		var pos = new google.maps.LatLng(app.lat, app.lng);
	    		app.marker.setPosition(pos);
	    		app.map.setCenter(pos);
	    	}
    	});
    	$('.link-geocode').click(function(e){
            if ($('#address').val().trim() === '') return;
		    app.geocoder.geocode(
		    	{
		    		'address': $('#address').val(),
		    		bounds: app.map.getBounds()
		    	},
			    function(results, status) {
			      if (status == google.maps.GeocoderStatus.OK) {
			      	var loc = results[0].geometry.location;
			        app.map.setCenter(loc);
			        app.marker.setPosition(loc);
			        $('#lat').val(loc.lat);
			        $('#lng').val(loc.lng);
			      } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
					swal({
						title: "No results.",
						text: "Sorry, Google found no results. Check the address or use the map to enter coordinates.",
						type: "info",
						confirmButtonText: "OK!"
					});
			      } else {
			      	swal({
						title: "Problem!",
						text: "Sorry, Google returned the following error: " + status,
						type: "error",
						confirmButtonText: "OK!"
					});
			      }
			    }
			);

    		return false;
    	});

        app.$form.submit(function(e){
            if ($('#address').val().trim() === ''){
                var latlng = {lat: parseFloat($('#lat').val()), lng: parseFloat($('#lng').val())};
                var geo = new google.maps.Geocoder;
                geo.geocode({'location': latlng}, function(results, status){
                    if (status === google.maps.GeocoderStatus.OK && results[0]) {
                        var out = results[0].address_components[0].long_name + ' ' + results[0].address_components[1].long_name;
                        $('#address').val(out);
                    }
                });
            }
        });
    },

    addTags: function(){
        app.tags.sort(function(a, b){
            if (a.name < b.name) return -1;
            if (b.name < a.name) return 1;
            return 0;
        });

        var numColumns = app.$tags.children('.column').length,
            $column = app.$tags.children('.column').first(),
            columnCount = 1,
            perColumn = Math.round(app.tags.length / numColumns),
            $clearButton = $('.clear-tags');

        for (var i = 0; i < app.tags.length; i++){
            var tag = app.tags[i],
                $li = $('<div class="checkbox"/>'),
                $label = $('<label><input type="checkbox" id="'+tag.id+'" data-name="'+tag.name+'"> '+tag.name+'</label>').appendTo($li),
                $check = $label.find('input');

            $check.change(function(){
                app.$tags.find(':checked').length == 0 ? $clearButton.addClass('hidden') : $clearButton.removeClass('hidden');
            });

            if (i >= perColumn * columnCount && columnCount < numColumns){
                $column = $column.nextAll('.column').first();
                columnCount++;
            }
            $li.appendTo($column);
        }
    }


});