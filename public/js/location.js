// check for Geolocation support
if (navigator.geolocation) {
	var position;
	var geoOptions = {
		maximumAge: 5 * 60 * 1000,
		timeout: 10 * 1000
	}
	var geoSuccess = function(pos) {
		position = pos;
		var lat = position.coords.latitude;
		var lng = position.coords.longitude;
		$.getJSON('nearme/'+lat+'/'+lng, function(data){
			console.log(data);
		});
	};
	var geoError = function(position) {
		console.log('Error occurred. Error code: ' + error.code);
		// error.code can be:
		//   0: unknown error
		//   1: permission denied
		//   2: position unavailable (error response from location provider)
		//   3: timed out
	};
	navigator.geolocation.getCurrentPosition(geoSuccess, geoError, geoOptions);
}
else {
  console.log('Geolocation is not supported for this Browser/OS version yet.');
}

