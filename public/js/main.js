'use strict';

if (!window.console) window.console = {};
if (!window.console.log) window.console.log = function () { };

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="token"]').attr('content')
    }
});

var	$window = $(window);

var app = {
    defaultLat: 45.5236111,
    defaultLng: -122.675,
    $geoReady: $.Deferred()
};

app.lat = app.defaultLat;
app.lng = app.defaultLng;

google.maps.event.addDomListener(window, 'load', function(){
	if (typeof app.initialize !== 'undefined'){
		app.initialize();
	}
});

app.getGeo = function(){
    var timeout = 4000;

    if (navigator && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function(pos){
                app.hasGeo = true;
                app.lat = pos.coords.latitude;
                app.lng = pos.coords.longitude;
                app.$geoReady.resolve();
            },
            function(error){
                app.hasGeo = false;
                app.$geoReady.resolve();
            },
            {
                maximumAge: 5 * 60 * 1000,
                timeout: timeout
            }
        );
    } else {
        app.hasGeo = false;
        app.$geoReady.resolve();
    }


    //for situations where user closes permission box but doesn't fire error (firefox)
    setTimeout(function () {
        if (app.$geoReady.state() !== "resolved") {
            app.hasGeo = false;
            app.$geoReady.notify();
        }
    }, timeout + 1000);


    return app.$geoReady;
};
