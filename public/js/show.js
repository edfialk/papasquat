Dropzone.options.dropzone = {
  paramName: "file", // The name that will be used to transfer the file
  maxFilesize: 2, // MB
  headers: {
	'X-CSRF-Token': $('meta[name="token"]').attr('content')
  }
};

app = $.extend(app, {

	initialize: function(){

		var $stars = $('.ratings_stars');
		app.set_votes($stars.parent());
		var rating = $stars.data('rating');
		var votes = $stars.data('votes');
		var lat = parseFloat($('#location-lat').val().trim());
		var lng = parseFloat($('#location-lng').val().trim());
		var mapOptions = {
			center: { lat: lat, lng: lng },
			zoom: 15
		};

		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

		new google.maps.Marker({
			position: new google.maps.LatLng(lat, lng),
			map: map,
			icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
		});

		app.getGeo().done(function(){
			new google.maps.Marker({
				position: new google.maps.LatLng(app.lat, app.lng),
				map: map,
				icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
			});
		});

		$stars.hover(
		    function() {
		    	var t = $(this);
		    	t.parent().children().removeClass('ratings_over ratings_vote');
		        t.prevAll().andSelf().addClass('ratings_over');
		        t.nextAll().removeClass('ratings_vote');
		        t.parent().data('vote', t.prevAll().length + 1);
		    },
		    function() {
		    	var t = $(this);
		        t.prevAll().andSelf().removeClass('ratings_over');
		        app.set_votes(t.parent());
		    }
		);

		$stars.click(function() {
			if ($('#redirectLoc').length > 0){
				window.location = '/auth/login?redirect='+$('#redirectLoc').val();
			}
		    var star = this;
		    var widget = $(this).parent();
		    var clicked_data = {
		        vote : widget.data('vote'),
		        id : widget.attr('id')
		    };
		    $.post(window.location+'/rate', clicked_data)
		    	.done(function(data) {
			        widget.data('rating',  data.rating);
			        widget.data('votes', data.votes );
			        $('#vote').val(widget.data('vote'));
			        app.set_votes(widget);
			    })
			    .fail(function(data) {
			    	console.log(data);
			    });
		});

		$('.pictures img').click(function(e){
			// e.preventDefault();
			// console.log('should be zooming on '+$(e.target).attr('data-id'));
		});


		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			$('#submit-web-address').toggleClass('hidden', e.target.text != 'web address');
		});

		$('#submit-web-address').click(function(e){
			var $form = $('#tab-web-address form');
			var url = $form.attr('action');
			$('.status').addClass('hidden');

			if (!$form[0].checkValidity()){
				$('.status').html('Invalid web address').removeClass('hidden');
				$('#url').focus();
				return false;
			}

			$.post(url, {url: $('#url').val()})
				.done(function(){
					location.reload();
				})
				.fail(function(jqXHR, textStatus, errorThrown){
					$('.status').html('Sorry, there seems to have been a problem: ' + textStatus).removeClass('hidden');
				});
		});

		app.initGallery();

	},

	set_votes: function(widget) {

	 	var widget = $(widget);
	    var vote = $('#vote').val();
	    var vote_class = 'ratings_vote';
	    var exact = Math.round(widget.data('rating') * 100) / 100;
	    var votes = widget.data('votes');
	    var stars = Math.round(exact);

	    if (vote != ""){
	    	vote_class = 'ratings_over';
	    	stars = vote;
	    }

	 	widget.children().removeClass('ratings_vote ratings_over');
	    widget.find('.star_' + stars).prevAll().andSelf().addClass(vote_class);
	    widget.find('.star_' + stars).nextAll().removeClass('ratings_vote');
	    widget.find('.total_votes').text( votes + ' votes recorded (' + exact + ' rating)' );
	},

	initGallery: function(){
		$('.gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title') ? item.el.attr('title') : '';
				}
			}
		});
	}

});