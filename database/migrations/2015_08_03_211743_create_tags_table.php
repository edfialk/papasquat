<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('tag'))
        {
            Schema::create('tag', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->string('name');
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('point_tag'))
        {
            Schema::create('point_tag', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->integer('point_id')->unsigned();
                $table->integer('tag_id')->unsigned();
                $table->timestamps();
                $table->foreign('point_id')->references('id')->on('points')->onDelete('cascade');
                $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('point_tag', function($table)
        {
            $table->dropForeign('point_tag_point_id_foreign');
            $table->dropForeign('point_tag_tag_id_foreign');
        });
        Schema::drop('point_tag');
        Schema::drop('tags');
    }
}
