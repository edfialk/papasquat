<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('point')){
            Schema::drop('point');
        }
        Schema::create('point', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('city');
            $table->string('address');
            $table->text('description');
            $table->string('website');
            $table->double('rating');
            $table->integer('votes')->unsigned();
            $table->double('lat', 8, 5);
            $table->double('lng', 8, 5);
            $table->string('slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('point')){
            Schema::drop('point');
        }

    }
}
