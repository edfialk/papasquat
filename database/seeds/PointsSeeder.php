<?php

use Illuminate\Database\Seeder;

class PointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    factory('App\Point', 500)
	    	->create()
	    	->each( function($u) {
	    		for ($i = 0; $i < rand(1, 5); $i++){
		    		$t = App\Tag::orderByRaw('RAND()')->first();
		        	$u->tags()->attach($t);
	    		}
	    	}
	    );
    }
}
