<?php

use Illuminate\Database\Seeder;

use App\Point;
use App\Picture;
use App\Tag;

class ParkSeeder extends Seeder
{

	protected $base_url = "http://www.portlandoregon.gov";

	protected $acceptableTags = ['picnic tables', 'fountain', 'paths - unpaved', 'paths - paved', 'statue or public art', 'historical site'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	libxml_use_internal_errors(true);

		$url = "http://www.portlandoregon.gov/parks/finder/index.cfm?ShowResults=yes";

		$xpath = $this->getXPath($url);
		$nlist = $xpath->query("//*[@class='searchParkBlk']//a[@class='sheader']");

		foreach($nlist as $n){

			$name = $n->textContent;
			$link = $this->base_url.$n->getAttribute('href');

			if (Point::locatedAt('Portland', $name)->count() > 0){
				$point = Point::locatedAt('Portland', $name)->first();
				$point->website = $link;
				$point->save();
				// echo "skipping: ".$name."\n";
				// continue;
			}

			continue;

			$amenities = $xpath->query('./div[@class="amenityblk"]/img', $n->parentNode);
			$tags = [];
			$keep = false;
			foreach($amenities as $amenity){
				$tag = $amenity->getAttribute('title');
				array_push($tags, $amenity->getAttribute('alt'));
				if (array_search($tag, $this->acceptableTags) !== false){
					$keep = true;
					break;
				}
			}

			if (stripos($name, 'park') !== false) $keep = true;

			if (!$keep) continue;

			$page_xpath = $this->getXPath($link);
			$right = $page_xpath->query('//div[@id="parkright"]//div[@class="sheader"]')->item(0);
			$address = $right->childNodes->item(0)->textContent;
			$latlng = $this->getLatLng($page_xpath);
			$description = $page_xpath->query('//div[@id="parkright"]//div[@class="sheader"]');
			$found = false;
			foreach($description as $d){
				if (stripos($d->textContent, "Amenities") !== false){
					$description = $d;
					$found = true;
					break;
				}
				//text()[contains(., "Amenities")]')->item(0); //returns DOMText
			}
			if (!$found){
				$description = '';
			}else{
				if ($description->nextSibling){
					$description = $description->nextSibling;
				}
				if ($description->nextSibling){
					$description = $description->nextSibling;
				}
				$description = $description->textContent;
				$description = preg_replace('/[ \t]+/', ' ', preg_replace('/[\r\n]+/', "", $description));
				$description = trim($description);

			}

			$point = new Point;
			$point->name = $name;
			$point->lat = $latlng['lat'];
			$point->lng = $latlng['lng'];
			$point->city = 'Portland';
			$point->address = $address;
			$point->description = $description;
			$point->save();

			foreach($tags as $tag){
				$tag = Tag::firstOrCreate(['name' => $tag]);
				$point->tags()->save($tag);
			}

			$pictures = $page_xpath->query('//div[@class="photo"]/img');
			foreach($pictures as $pic){
				$url = $pic->getAttribute('src');
				$caption = $pic->getAttribute('alt');
				$pic = new Picture;
				$pic->filename = $this->base_url.$url;
				$pic->caption = $caption;
				$pic->point_id = $point->id;
				$pic->user_id = 1;
				$pic->save();
			}
		}
    }

    public function getXPath($url)
    {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$html = curl_exec($ch);
		curl_close($ch);

		$doc = new DOMDocument();
		@$doc->loadHTML($html);
		$xpath = new DOMXpath($doc);
		return $xpath;
    }

    public function getLatLng($xpath)
    {
		$viewmap = $xpath->query('//div[@id="parkright"]//div[@class="sheader"]/a')->item(0);
		$viewmap_url = $viewmap->getAttribute('href');
		preg_match_all("#javascript:createWin\(\'([^\']+)\'#", $viewmap_url, $matches);
		$viewmap_url = $matches[1][0];

		$xpath = $this->getXPath($this->base_url.$viewmap_url);
		$latlng = $xpath->query('//form[@id="dirForm"]')->item(0);
		$latlng = $latlng->getAttribute('onsubmit');
		$latlng = str_replace('return directions(', '', $latlng);
		$latlng = str_replace(');', '', $latlng);
		$latlng = explode(",", $latlng);
		$lat = $latlng[0];
		$lng = $latlng[1];
		return ['lat' => $lat, 'lng' => $lng];
    }
}
