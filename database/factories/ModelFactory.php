<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Point::class, function (Faker\Generator $faker) {
	return [
		'city' => 'Portland',
		'name' => $faker->name,
		'address' => $faker->address,
		'description' => $faker->paragraph,
		'website' => $faker->url,
		'lat' => $faker->randomFloat(4, 45, 46),
        'lng' => $faker->randomFloat(4, -123, -122)
        //portland, or = 45.5200° N, 122.6819
	];
});

$factory->define(App\Tag::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->word
	];
});
